
name := "play-api-demo"

scalaVersion := "2.12.4"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala, DebianPlugin)

libraryDependencies ++= Seq(
  guice
)

// Configuration entries for packaging
maintainer in Linux := "Sherlock Holmes <sherlock.holmes@baker.street>"

packageSummary in Linux := "Demo Play application"

packageDescription := "Demo Play application"

daemonUser in Linux := "sherlock"

daemonGroup in Linux := "sherlock"

// Don't include source files in production bundle
sources in (Compile, doc) := Seq.empty

publishArtifact in (Compile, packageDoc) := false
