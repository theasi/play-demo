
# Remove artefact from previous runs
[ -f /usr/share/play-api-demo/RUNNING_PID ] && kill $(cat /usr/share/play-api-demo/RUNNING_PID)

# This is the script that actually runs the application in production
application_secret=$(pwgen -1 --secure 50)

play-api-demo -Dplay.http.secret.key=$application_secret
