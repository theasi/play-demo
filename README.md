# Developing Play APIs on Faculty

This demonstrates working with the
[Play framework](https://www.playframework.com/documentation/2.6.x/Documentation)
on Faculty. A working version of the API lives in the `play-demos` project.

## Development workflow

To develop the application:

1. Create a new server
2. Apply the environment `scala-dev-environment`. This installs OpenJDK 8 and
   sbt.
3. In a terminal (either through `sml shell` or the browser-based terminal),
   navigate to `/project/play-api`.
4. Start sbt by typing `sbt`
5. In the sbt prompt, type either `~compile` to continuously compile the app, or
   `run` to actually run a development server. The app is set to listen on
   port 8000.

You can find some sample queries in the `play-api-queries` directory.

## Building the application

The deployment workflow outlined above is suitable for development, but should
not be used to actually run the API in production. There are several ways of
actually building and bundling a Play application for production. We have found
that building a Debian artefact works well.

1. Navigate to `/project/play-apis`
2. Run `sbt`
3. Run `clean` in the SBT console to clean any existing artefacts
4. Run `debian:packageBin` to actually build a package.

This will build an artefact called
`target/play-api-demo_0.1-${VERSION}_all.deb`. You can test that this artefact
installs cleanly by running the following, replacing \$VERSION with the version
that you have just built.

```
sudo dpkg --install /project/play-api/target/play-api-demo_${VERSION}_all.deb
```

## Running the application in production

The environment `play-demo-prod` installs OpenJDK 8 and a specific version of
the application.

The API should have the following parameters:

- project: `/project/play-api`
- script: `run.sh`
- environments: `play-demo-prod`

## Faculty resources

`scala-dev-environment` should include:

- `openjdk-8-jdk` under APT
- the following script to install SBT

```
sudo curl -sLo /usr/local/bin/sbt https://raw.githubusercontent.com/paulp/sbt-extras/master/sbt
sudo chmod +x /usr/local/bin/sbt
```

`play-demo-prod` should include:

- `openjdk-8-jdk` and `pwgen` under APT
- the following script to install the API:

```
# Install the build artefact
VERSION=0.1-SNAPSHOT
sudo dpkg --install /project/play-api/target/play-api-demo_${VERSION}_all.deb

# Change ownership of runtime data directory to allow the application to
# run as a regular user, rather than as root
sudo chown -R sherlock:sherlock /usr/share/play-api-demo/
```
