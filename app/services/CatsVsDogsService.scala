package services

import javax.inject.Inject

import scala.concurrent.Future

import models.{Person, PredictionValue, Prediction}

class CatsVsDogsService @Inject() {

  /** Determine whether a person is a cat or dog person
    *
    * We return a future to simulate some asynchronous operation.
    */
  def predict(person: Person): Future[Prediction] = {
    val prediction = person.name.toLowerCase match {
      case "schrodinger" | "schroedinger" =>
        Prediction(PredictionValue.CatPerson, 0.9)
      case _ =>
        Prediction(PredictionValue.DogPerson, 0.6)
    }
    Future.successful(prediction)
  }

}
