package controllers

import play.api.libs.json.Json
import play.api.libs.json.Writes

import models.Prediction

private[controllers] trait ControllerWrites {

  val predictionWrites = Writes[Prediction] {
    case Prediction(predictionValue, probability) => Json.obj(
      "prediction" -> predictionValue,
      "probability" -> probability
    )
  }

}
