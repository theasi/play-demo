package controllers

import scala.concurrent.{ExecutionContext, Future}

import javax.inject.Inject

import play.api.Logger
import play.api.mvc._
import play.api.libs.json.{Json, JsSuccess, JsError, JsValue}

import models.Person
import services.CatsVsDogsService

class CatsVsDogsController @Inject() (
  catsVsDogsService: CatsVsDogsService,
  cc: ControllerComponents,
  implicit val ec: ExecutionContext
) extends AbstractController(cc) with ControllerWrites with ControllerReads {

  val log = Logger("cats-vs-dogs")

  def prediction = Action.async(parse.json) { request =>
    ifRequestBodyWellFormed(request.body) { person =>
      catsVsDogsService.predict(person).map { prediction =>
        log.info(
          s"Predicting that ${person.name} is a ${prediction.value} " +
            s"with probability ${prediction.probability}.")
        Ok(Json.toJson(prediction)(predictionWrites))
      }
    }
  }

  private def ifRequestBodyWellFormed
    (body: JsValue)(block: Person => Future[Result]): Future[Result] =
    body.validate(personReads) match {
      case s: JsSuccess[Person] =>
        block(s.get)
      case e: JsError =>
        val errorAsJson = JsError.toJson(e)
        log.info(s"Received badly formatted request: $errorAsJson")
        Future.successful(BadRequest(Json.obj("error" -> errorAsJson)))
  }

}
