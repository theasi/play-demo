package controllers

import play.api.libs.json.{Json, JsPath}
import play.api.libs.json.Reads
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

import models.Person

private[controllers] trait ControllerReads {

  private val nameReads: Reads[String] =
    JsPath.read[String](
      minLength[String](1) keepAnd maxLength[String](50)
    )

  val personReads: Reads[Person] =
    (JsPath \ "name").read(nameReads).map { name => Person(name) }

}
