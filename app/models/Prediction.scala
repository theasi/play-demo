package models

case class Prediction(
  value: PredictionValue.Value,
  probability: Double
)
