package models

object PredictionValue extends Enumeration {
  val DogPerson = Value("dog-person")
  val CatPerson = Value("cat-person")
}
